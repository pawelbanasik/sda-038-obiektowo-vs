package com.pawelbanasik;

public class Main {

	public static void main(String[] args) {

		Serie serie = new SimpleSerie();
		System.out.println(serie.sum(5));

		Serie constantSerie = new Serie() {
			// anonimowa (pokazal roznice jak inaczej wywola co sie dzieje
			@Override
			public int generate(int i) {
				return 5;
			}

		};
		System.out.println(constantSerie.sum(5));
		Serie some = new SomeSerie();
		System.out.println(some.sum(5));

	}

	// klasa wewnetrzne
	// malo sie uzywa
	// klasy moga byc statyczne
	// zeby nie robic osobnego pliku np. robie wewnatrz tego jednego pliku
	static class SomeSerie extends Serie {

		@Override
		public int generate(int i) {

			return i + 10;
		}

	}

}
